from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.core.mail import EmailMessage


class MyRegistrationForm(UserCreationForm):
    email = forms.EmailField(required=True)

    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2')

    def save(self, commit=True):
        user = super(MyRegistrationForm, self).save(commit=False)
        user.is_active=False;
        # user.email = self.cleaned_data['email']
        # email = EmailMessage('Hello', 'World', to=['p.zawadzki.pz@gmail.com'])
        # email.send()

        if commit:
            user.save()
        return user
