from django.conf.urls import patterns, include, url
from django.contrib import admin
admin.autodiscover()#wykrywa modele
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = patterns('',
    url(r'^blog/', include('microblog.urls')),
    url(r'^blog/\S+/$', 'microblog.views.blad'),
    #url dla modulu admin
    url(r'^admin/', include(admin.site.urls)),
    #urle do logowania/ rejestrowania sie
    url(r'^accounts/login/$', 'mysite.views.login' ),
    url(r'^accounts/auth/$', 'mysite.views.auth_view' ),
    url(r'^accounts/logout/$', 'mysite.views.logout' ),
    url(r'^accounts/loggedin/$', 'mysite.views.loggedin' ),
    url(r'^accounts/invalid/$', 'mysite.views.invalid_login' ),
    url(r'^accounts/register/$', 'mysite.views.register_user' ),
    url(r'^accounts/register_success/$', 'mysite.views.register_success' ),

    url(r'^accounts/activate/(?P<username>)/(?P<token>$)', 'mysite.views.activate_user' ),

)

urlpatterns += staticfiles_urlpatterns()