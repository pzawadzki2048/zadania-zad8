from django.contrib import admin
from models import Wpis, Tag


class WpisAdmin(admin.ModelAdmin):
    list_display = ('tytul', 'data_publikacji',)
    list_filter = ('tags__nazwa',)
    ordering = ('data_publikacji', )
    search_fields = ('tags__tagi',)


class TagAdmin(admin.ModelAdmin):
    list_display = ('nazwa',)

admin.site.register(Wpis, WpisAdmin)
admin.site.register(Tag, TagAdmin)# rejestruje wpis w systemie admina

