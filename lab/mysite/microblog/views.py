from django.shortcuts import render_to_response
from webob.descriptors import parse_int
from form import WpisForm
from models import Wpis, Tag
from django.http import HttpResponseRedirect
from django.core.context_processors import  csrf
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.utils import timezone
from django.shortcuts import get_object_or_404
from django.http import HttpResponseForbidden
from django.contrib.admin.views.decorators import staff_member_required

from django.template import RequestContext

from django.utils import timezone



def edit(request, wpis_id):

        error = []

        wpis = get_object_or_404(Wpis, id=wpis_id)
        czas = wpis.zmiany_data
        czas2 = timezone.now()
        czas = (czas2 - czas).seconds
        czas = czas/100



        if request.user.is_staff or czas < 10:
            initial = {'tytul': wpis.tytul, 'tresc': wpis.tresc, 'tags': wpis.tags.all()}
            if request.POST:
                form = WpisForm(request.POST, instance=wpis,)
                if form.is_valid():
                    wpis_form = form.save(commit=False)
                    wpis_form.zmiany_data = timezone.now()
                    wpis_form.zmiany_podpis = request.user.username
                    wpis_form.save()
                    return HttpResponseRedirect('/~p11/mysite.wsgi/blog/')
                else:
                    error.append('Zle wypelniles formularz')

            form = WpisForm(initial=initial)
            args = {}
            args.update(csrf(request))
            args['wiad'] = error
            args['wpis_id'] = wpis_id
            args['form'] = form

            return render_to_response('edycja.html', args,
                                            context_instance=RequestContext(request),)
        return HttpResponseRedirect('/~p11/mysite.wsgi/blog')



@login_required()
def create(request):

    podpisik = Wpis(podpis=request.user.username, data_publikacji=timezone.now(), zmiany_data=timezone.now(),)
    if request.POST:
        form = WpisForm(request.POST, instance=podpisik,)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/~p11/mysite.wsgi/blog')

        else:
            form = WpisForm()
            args = {}
            args.update(csrf(request))
            args['wiad'] = "Zle wypelniles formularz"
            args['form'] = form
            return render_to_response('create_article.html', args,
                                context_instance=RequestContext(request),)
    else:
        form = WpisForm()
        args = {}
        args.update(csrf(request))

        args['form'] = form
        return render_to_response('create_article.html', args,
                                context_instance=RequestContext(request),)


def Articles(request):
    return render_to_response('index.html',
        {'articles' : Wpis.objects.all().order_by("-zmiany_data"),
         'tags' : Tag.objects.all()},
        context_instance=RequestContext(request),)

def Articles_filter(request):
    return render_to_response('index.html',
        {'articles' : Wpis.objects.filter(podpis = request.user.username).order_by("-zmiany_data")},
        context_instance=RequestContext(request),)

def listByTags(request, tag):
    return render_to_response('index.html',
        {'articles' : Wpis.objects.filter(tags=tag).order_by("-zmiany_data"),
         'tags' : Tag.objects.all()},
        context_instance=RequestContext(request),)

def Article(request, wpis_id = 1):
    try:
        return render_to_response('article.html',
            {'wpis' : Wpis.objects.get(id = wpis_id ) },
                    context_instance=RequestContext(request),)
    except:
        return HttpResponseRedirect('/~p11/mysite.wsgi/blog/blad')

def blad(request):
    return render_to_response('error.html',
                            context_instance=RequestContext(request),)