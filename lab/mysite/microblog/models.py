from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Tag(models.Model):
    nazwa = models.CharField(max_length=20)

    def __unicode__(self):
        return self.nazwa



class Profile(models.Model):
    user = models.OneToOneField(User)


class Wpis(models.Model):
    tytul = models.CharField(max_length=200)
    tresc = models.TextField(max_length=2000)
    podpis = models.CharField(max_length=50)
    tags = models.ManyToManyField(Tag)
    data_publikacji = models.DateTimeField('termin publikacji')
    zmiany_data = models.DateTimeField('termin zmian publikacji')
    zmiany_podpis = models.CharField(max_length=50)

    def __unicode__(self):
        return self.tytul

