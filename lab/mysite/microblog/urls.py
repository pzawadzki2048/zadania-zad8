from django.conf.urls import patterns, url
#from django.views.generic import TemplateView
from django.http import HttpResponse
from django.views.generic import ListView, DetailView



#urlpatterns = patterns('',
 #   url(r'^$', TemplateView.as_view(template_name='microblog/index.html'))
#)
from django.conf.urls import patterns, url
from django.http import HttpResponse
from django.views.generic import ListView, DetailView

from models import Wpis
import views


urlpatterns = patterns('',
    url(r'^$', 'microblog.views.Articles', name="main"),
    url(r'^get/(?P<wpis_id>\d+)/$', 'microblog.views.Article'),
    url(r'^filter/$', 'microblog.views.Articles_filter', name='blogfilter'),
    url(r'^create/$', 'microblog.views.create'),
    url(r'^bytag/(?P<tag>.*)/$', views.listByTags, name='listByTags'),
    url(r'^edit_view/(?P<wpis_id>\d+)/$', 'microblog.views.edit'),
    url(r'^edit/$', 'microblog.views.edit'),
    url(r'^blad/$', 'microblog.views.blad', name="blogblad"),
)