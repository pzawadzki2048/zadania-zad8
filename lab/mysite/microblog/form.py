from django import forms
from models import Wpis, Tag

class WpisForm(forms.ModelForm):
    tresc = forms.CharField(label='Tresc', max_length=200, min_length=10)
    zmiany_data = forms.DateTimeField(required=False, widget=forms.HiddenInput())
    zmiany_podpis = forms.CharField(required=False, widget=forms.HiddenInput())

    class Meta:
        model = Wpis
        fields = ('tytul', 'tresc', 'tags')

class TagForm(forms.ModelForm):
    nazwa = forms.CharField(max_length=20)

    class Meta:
        model = Tag
