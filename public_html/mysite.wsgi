import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "mysite.settings")

import sys
sys.path.insert(0, '/home/p11/lab/mysite')
activate_this = '/home/p11/djangoenv/bin/activate_this.py'
execfile(activate_this, dict(__file__=activate_this))

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()